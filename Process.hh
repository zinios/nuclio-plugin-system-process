<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\system\process
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	
	/**
	 * Process plugin for spawing new HHVM instances in the background.
	 * 
	 * Can be used either synchronously or asynchronously.
	 * 
	 * @package nuclio\plugin\system\process
	 */
	<<factory>>
	class Process extends Plugin
	{
		/**
		* Static method to create an instance of Process class 
		*
		* Create an instance of Process class if there is no existing one
		* 
	 	* @access Public
		*
		* @return     Process an instance of Process class
		*/
		public static function getInstance(/* HH_FIXME[4033] */...$args):Process
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		/**
		 * @var        string $processInstance 	This is created when the process is opened
		 * 
		 * @access private
		 **/
		private resource $processInstance;
		
		/**
		 * @var        array<array<string>> $pipeDesc 	Holds different pipes for input, output and error logging.
		 * 
		 * @access private
		 **/
		private array<array<string>> $pipeDesc=
		[
			['pipe','r'],						//Standard Input
			['pipe','w'],						//Standard Output
			['pipe','w'],						//Standard Error
			// ['file','/var/log/nuclio/process.out.log','a']		//Standard Error
		];
		
		/**
		* @var        Vector<resource> $pipes 	Holds pipes value
		* 
		* @access private
		**/
		private Vector<resource> $pipes;
		
		/**
		 * Process class constructor 
		 *
		 * This function sets up the process plugin. Configures the pipe and
		 * pipe desc vars, then executes the given command.
		 * 
		 * @access public
		 * 
		 * @param      string $command process command
		 * @param      Vector<string> $args list of command arguments
		 * @param      bool $async flag to indicate sync or async method call
		 * 
		 * @return mixed
		 */
		public function __construct(string $command,Vector<string> $args=new Vector(null),bool $async=true)
		{
			parent::__construct();
			$this->pipes=new Vector(null);
			$cmd=$command.' '.implode(' ',$args);
			$this->processInstance=proc_open($cmd,$this->pipeDesc,&$this->pipes,realpath('.'));
		}
		
		/**
		 * Process class destructor
		 * 
		 * close the pointers to instance and pipe objects
		 * 
		 * @access public
		 */
		public function __destruct():void
		{
			fclose($this->pipes[0]);
			fclose($this->pipes[1]);
			proc_close($this->processInstance);
		}
		
		/**
		* Gets process instance object
		* 
		* @return resource Current instacen process
		*/
		public function getResource():resource
		{
			return $this->processInstance;
		}
		
		/**
		 * Gets stream contents
		 * 
		 * Get stream content from pipes vector index 1
		 *
		 * @return string stream content
		 */
		public function getOutput():string
		{
			return stream_get_contents($this->pipes[1]);
		}
		
		public function getError():string
		{
			return stream_get_contents($this->pipes[2]);
		}
	}
}
